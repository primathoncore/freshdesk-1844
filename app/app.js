$(document).ready(function() {
	app.initialized().then(function(_client) {
		var client = _client;
		client.events.on('ticket.propertiesUpdated', function(event) {
			client.data
				.get('ticket')
				.then(function(data) {
					var event_data = event.helper.getData();
					client.data.get('loggedInUser').then(userData => {
						let agent = {};
						if(userData && userData.loggedInUser && userData.loggedInUser.contact){
							agent['name'] = userData.loggedInUser.contact.name;
							agent['email'] = userData.loggedInUser.contact.email;
						}
						let args = {
							ticketId: data.ticket.id,
							updatedFields: event_data.changedAttributes,
							updatedBy: agent,
						};
						client.request.invoke('syncTicket', args).then(
							function() {
								console.log(JSON.stringify(response));
							},
							function(err) {
								console.log("Error occurred while invoking syncTicket",JSON.stringify(err));
							}
						);
					});
				})
				.catch(function(e) {
					console.log('Unknown error occurred', JSON.stringify(e));
				});
		});
	});
});
